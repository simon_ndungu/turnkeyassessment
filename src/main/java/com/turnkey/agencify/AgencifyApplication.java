package com.turnkey.agencify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgencifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgencifyApplication.class, args);
	}

}
