/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.data;

import lombok.Data;

/**
 *
 * @author Simon.waweru
 */
@Data
public class UserDTO {

    private String surname;
    private String otherNames;
    private String gender;
    private String dob;
    private String idNumber;
    
}
