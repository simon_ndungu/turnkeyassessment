/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.data;

/**
 *
 * @author Simon.waweru
 */
public enum AccountStatus {
    ACTIVE,
    DORMANT
}
