/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.data;

import com.turnkey.agencify.security.domain.Auditable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.Data;

/**
 *
 * @author Simon.waweru
 */
@Data
public class AccountDetailsDTO {

    private String accountNumber;
    private String accountCode;

    @Enumerated(EnumType.STRING)
    private AccountStatus accountStatus;
}
