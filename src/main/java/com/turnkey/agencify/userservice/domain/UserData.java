/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.domain;

import com.turnkey.agencify.security.domain.Auditable;
import com.turnkey.agencify.userservice.data.UserDTO;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import lombok.Data;

/**
 *
 * @author Simon.waweru
 */
@Data
@Entity
public class UserData extends Auditable {

    @OneToMany(mappedBy = "user")
    private List<AccountDetails> accountDetailss;

    private String surname;
    private String otherNames;
    private String gender;
    private String dob;
    private String idNumber;

    public UserDTO toData() {
        UserDTO dto = new UserDTO();
        dto.setDob(this.getDob());
        dto.setGender(this.getGender());
        dto.setIdNumber(this.getIdNumber());
        dto.setOtherNames(this.getOtherNames());
        dto.setSurname(this.getSurname());
        return dto;
    }

    
}
