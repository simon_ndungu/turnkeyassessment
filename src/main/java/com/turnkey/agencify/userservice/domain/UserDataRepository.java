/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.domain;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Simon.waweru
 */
@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {

    Page<UserData> findByGender(final String gender, final Pageable pageable);
    
    Optional<UserData> findByIdNumber(String idNumber);
}
