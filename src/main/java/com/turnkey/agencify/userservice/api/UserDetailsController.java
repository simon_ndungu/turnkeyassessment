/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.api;

import com.turnkey.agencify.userservice.data.UserDTO;
import com.turnkey.agencify.userservice.domain.UserData;
import com.turnkey.agencify.userservice.service.UserDetailsService;
import io.swagger.annotations.Api;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Simon.waweru
 */
@RequiredArgsConstructor
@Api
@RestController
@RequestMapping("/api")
public class UserDetailsController {

    private final UserDetailsService userDetailsService;

    @PostMapping("/user")
    public ResponseEntity<UserDTO> addUserDetails(@RequestBody UserDTO userDTO) {
        UserData userData = userDetailsService.createUserData(userDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(userData.toData());
    }

    @GetMapping("/user")
    public ResponseEntity<List<UserDTO>> fetchAllUsers() {
        List<UserDTO> users = userDetailsService.fetchAllUsers().stream().map(u -> u.toData())
                .collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(users);
    }

    @GetMapping("/user/{idNumber}")
    public ResponseEntity<UserDTO> fetchUserDetailsByIdNumber(@PathVariable("idNumber") final String idNumber) {
        UserData userData = userDetailsService.fetchUserByIdNumber(idNumber);
        return ResponseEntity.status(HttpStatus.OK).body(userData.toData());
    }
}
