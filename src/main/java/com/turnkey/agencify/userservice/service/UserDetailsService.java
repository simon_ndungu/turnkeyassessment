/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.service;

import com.turnkey.agencify.exeption.APIException;
import com.turnkey.agencify.userservice.data.UserDTO;
import com.turnkey.agencify.userservice.domain.UserData;
import com.turnkey.agencify.userservice.domain.UserDataRepository;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Simon.waweru
 */
@Service
@AllArgsConstructor
@Slf4j
public class UserDetailsService {

    private final UserDataRepository userDataRepository;

    public UserData createUserData(UserDTO userDTO) {
        log.info("Start save userdetails");

        return userDataRepository.save(toEntity(userDTO));
    }

    public UserData fetchUserByIdNumber(String idNumber) {
        log.info("Start find user by id");
        return userDataRepository.findByIdNumber(idNumber).orElseThrow(() -> APIException.notFound("User identified by id number {0} not found ", idNumber));
    }

    public List<UserData> fetchAllUsers() {
        return userDataRepository.findAll();
    }

    public Page<UserData> fetchAllUsersByGender(String gender, Pageable pageable) {
        return userDataRepository.findByGender(gender, pageable);
    }

    public UserData toEntity(UserDTO dto) {
        UserData entity = new UserData();
        entity.setDob(dto.getDob());
        entity.setGender(dto.getGender());
        entity.setIdNumber(dto.getIdNumber());
        entity.setOtherNames(dto.getOtherNames());
        entity.setSurname(dto.getSurname());
        return entity;
    }
}
