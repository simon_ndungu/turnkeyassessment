/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turnkey.agencify.userservice.api;

import com.turnkey.agencify.userservice.data.UserDTO;
import com.turnkey.agencify.userservice.service.UserDetailsService;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author Simon.waweru
 */
public class UserDetailsControllerTest {
    
    private static UserDetailsService userDetailsService;
    
    public UserDetailsControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        userDetailsService = Mockito.mock(UserDetailsService.class);
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addUserDetails method, of class UserDetailsController.
     */
    @Test
    public void testAddUserDetails() {
        System.out.println("addUserDetails");
        UserDTO userDTO = Mockito.mock(UserDTO.class);
        UserDetailsController instance = Mockito.mock(UserDetailsController.class);
        ResponseEntity<UserDTO> expResult = Mockito.mock(ResponseEntity.class);
        Mockito.when(instance.addUserDetails(userDTO)).thenReturn(expResult);
        
        ResponseEntity<UserDTO> result = instance.addUserDetails(userDTO);
        assertEquals(expResult, result);
    }

    /**
     * Test of fetchAllUsers method, of class UserDetailsController.
     */
    @Test
    public void testFetchAllUsers() {
        System.out.println("fetchAllUsers");
        UserDetailsController instance = Mockito.mock(UserDetailsController.class);
        ResponseEntity<List<UserDTO>> expResult = Mockito.mock(ResponseEntity.class);
        Mockito.when(instance.fetchAllUsers()).thenReturn(expResult);
        ResponseEntity<List<UserDTO>> result = instance.fetchAllUsers();
        assertEquals(expResult, result);
    }

    /**
     * Test of fetchUserDetailsByIdNumber method, of class UserDetailsController.
     */
    @Test
    public void testFetchUserDetailsByIdNumber() {
        System.out.println("fetchUserDetailsByIdNumber");
        String idNumber = "12345";
        UserDetailsController instance = Mockito.mock(UserDetailsController.class);
        ResponseEntity<UserDTO> expResult = Mockito.mock(ResponseEntity.class);
        when(instance.fetchUserDetailsByIdNumber(Mockito.any())).thenReturn(expResult);
        
        ResponseEntity<UserDTO> result = instance.fetchUserDetailsByIdNumber(idNumber);
        assertEquals(expResult, result);
    }
    
}
